module gitea.com/macaron/binding

go 1.11

require (
	gitea.com/macaron/macaron v1.3.3-0.20190803174002-53e005ff4827
	github.com/Unknwon/com v0.0.0-20190321035513-0fed4efef755
	github.com/smartystreets/goconvey v0.0.0-20190731233626-505e41936337
)
